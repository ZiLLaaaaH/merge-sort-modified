private void MergeSortedArray(long[] inputArray, int left, int mid, int right)
{
    int index = 0;
    int total_elements = right-left+1; //BODMAS rule
    int right_start = mid + 1;
    int temp_location = left;
    long[] tempArray = new long[total_elements];

    while ((left <= mid) && right_start <= right)
    {
        if (inputArray[left] <= inputArray[right_start])
        {
            tempArray[index++] = inputArray[left++];
        }
        else
        {
            tempArray[index++] = inputArray[right_start++];
        }
    }
    if (left > mid)
    {
        for(int j = right_start; j <= right; j++)
            tempArray[index++] = inputArray[right_start++];
    }
    else
    {
        for(int j = left; j <= mid; j++)
            tempArray[index++] = inputArray[left++];
    }
    //Array.Copy(tempArray, 0, inputArray, temp_location, total_elements);
    // just another way of accomplishing things (in-built copy)
    for (int i = 0, j = temp_location; i < total_elements; i++, j++)
    {
        inputArray[j] = tempArray[i];
    }
}